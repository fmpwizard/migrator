# migrator

Simple Go migration for PostgreSQL


## Env variables.

```
export DATABASE_URL="postgres://postgres@localhost/core?sslmode=disable&timezone=UTC"
```

## To run all migrations

```
go build   gitlab.com/fmpwizard/migrator/cmd/migrator && ./migrator -up
```

## To downgrade migrations, one at the time.

```
go build   gitlab.com/fmpwizard/migrator/cmd/migrator && ./migrator -down
```

## To create a migration

```
go build   gitlab.com/fmpwizard/migrator/cmd/migrator && ./migrator -new
```

This will query your database, look for the latest migration and then create a new template with the current version + 1
You need to manually add the migration to `main.go`:

```
var MigrationHistory = []*migrations.OpenlyMigration{
	&migrations.Migration000001,
	&migrations.Migration000002,
    // Keep adding migrations here.
}
```

### Misc notes.

After feedback from Matt, all migrations run inside a transaction, on error, we rollback all changes.

# Demo.

```
$ export DATABASE_URL="postgres://postgres@localhost/core?sslmode=disable&timezone=UTC"
$ go build   gitlab.com/fmpwizard/migrator/cmd/migrator && ./migrator -up
2021/03/10 23:20:40 schema is at version: 0
2021/03/10 23:20:40 about to run migration 1: "create dummy table"
2021/03/10 23:20:40 about to run migration 2: "Add dummy column"
2021/03/10 23:20:40 finished migration
$
$ go build   gitlab.com/fmpwizard/migrator/cmd/migrator && ./migrator -down
2021/03/10 23:20:49 schema is at version: 2
2021/03/10 23:20:49 about to undo migration 2: "Add dummy column"
2021/03/10 23:20:49 finished migration
$
$ go build   gitlab.com/fmpwizard/migrator/cmd/migrator && ./migrator -down
2021/03/10 23:20:53 schema is at version: 1
2021/03/10 23:20:53 about to undo migration 1: "create dummy table"
2021/03/10 23:20:53 finished migration
$
$ go build   gitlab.com/fmpwizard/migrator/cmd/migrator && ./migrator -down
2021/03/10 23:20:55 schema is at version: 0
2021/03/10 23:20:55 finished migration
$ go build   gitlab.com/fmpwizard/migrator/cmd/migrator && ./migrator -up
2021/03/10 23:21:24 schema is at version: 0
2021/03/10 23:21:24 about to run migration 1: "create dummy table"
2021/03/10 23:21:24 about to run migration 2: "Add dummy column"
2021/03/10 23:21:24 finished migration
$ go build   gitlab.com/fmpwizard/migrator/cmd/migrator && ./migrator -new
2021/03/10 23:21:27 schema is at version: 2
2021/03/10 23:21:27 created new migration at: cmd/migrator/migrations/000003.go
$ cat cmd/migrator/migrations/000003.go

package migrations

import (
	"context"

	"github.com/jackc/pgx/v4"
)

// Migration000003 is the variable we add to the migrationHistory slice of migrations to run
var Migration000003 = OpenlyMigration{
	Version: 3,
	Summary: "Add dummy column",
	Up:      up3,
	Down:    down3,
}

// up3 implements the upgrade func for the Migration interface
func up3(ctx context.Context, tx pgx.Tx) error {
	return nil
}

// down3 implements the downgrade func for the Migration interface
func down3(ctx context.Context, tx pgx.Tx) error {
	return nil
}

$
```