package pkg

import (
	"context"
	"errors"
	"fmt"
	"log"
	"time"

	"gitlab.com/fmpwizard/migrator/cmd/migrator/migrations"
	"gitlab.com/fmpwizard/migrator/pkg/db"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

// GetLatestCompletedMigration first creates the migration table to log migrations, if not present already.
// And then returns the current completed migration version
func GetLatestCompletedMigration(ctx context.Context, p *db.PGServer) (int, error) {
	if e := CreateOpenlyMigrationTable(ctx, p); e != nil {
		return 0, e
	}

	q := `SELECT version, dirty from openly_migrations ORDER BY version DESC LIMIT 1`
	row := p.DBPool.QueryRow(ctx, q)
	ver := 0
	dirty := false
	e := row.Scan(&ver, &dirty)
	if e != nil {
		// because the pgx package uses github.com/pkg/errors , we cannot just
		// e == pgx.ErrNoRows, that gives false
		// so we check the string error message, this should still work fine
		// if the pgx package changes their error message, as long as they use the same var
		if e.Error() == pgx.ErrNoRows.Error() {
			// No migrations, fresh DB
			return 0, nil
		}
		return 0, fmt.Errorf("failed to get latest migration version: %s", e)
	}

	if dirty {
		return 0, ErrDirty
	}

	return ver, nil
}

// CreateOpenlyMigrationTable create our migration table holding the versions run
func CreateOpenlyMigrationTable(ctx context.Context, p *db.PGServer) error {
	q := `
	CREATE TABLE IF NOT EXISTS openly_migrations (
		version bigint not null,
		dirty boolean not null,
		description varchar not null,
		CONSTRAINT openly_migrations_pkey PRIMARY KEY (version)
	)

	`

	_, e := p.DBPool.Exec(ctx, q)
	if e != nil {
		return fmt.Errorf("failed to create openly_migrations table: %s", e)
	}
	return nil
}

// UpdateMigrationStatus inserts or updates the status of the given migration version.
func UpdateMigrationStatus(ctx context.Context, tx pgx.Tx, ver int, summary string, done bool) error {
	if !done {
		sql := `INSERT INTO openly_migrations (version, dirty, description) VALUES ($1, $2, $3)`
		_, e := tx.Exec(ctx, sql, ver, true, summary)
		if e != nil {
			return fmt.Errorf("failed to insert openly_migrations entry: %s", e)
		}
		return nil
	}

	sql := `UPDATE openly_migrations SET dirty = $1 WHERE version = $2`
	_, e := tx.Exec(ctx, sql, false, ver)
	if e != nil {
		return fmt.Errorf("failed to update openly_migrations entry: %s", e)
	}

	return nil
}

// UpdateMigrationStatusDown is similar to UpdateMigrationStatus, but works for downgrade, the logic
// is different because we delete the most recent entry after the downgrade was run.
func UpdateMigrationStatusDown(ctx context.Context, tx pgx.Tx, ver int, summary string, done bool) error {
	if !done {
		sql := `UPDATE openly_migrations SET dirty = $1 WHERE version = $2`
		_, e := tx.Exec(ctx, sql, true, ver)
		if e != nil {
			return fmt.Errorf("failed to update openly_migrations entry: %s", e)
		}
		return nil
	}

	sql := `DELETE FROM openly_migrations  WHERE version = $1`
	_, e := tx.Exec(ctx, sql, ver)
	if e != nil {
		return fmt.Errorf("failed to remove openly_migrations entry: %s", e)
	}

	return nil
}

// RunMigration runs the upgrade migrations, in order.
func RunMigration(ctx context.Context, pgSrv *db.PGServer, ver int, migrationHistory []*migrations.OpenlyMigration) error {
	// TODO: this would take a real context from the client
	tx, e := pgSrv.DBPool.Begin(ctx)
	if e != nil {
		return fmt.Errorf("failed to begin transaction: %s", e)
	}
	defer tx.Rollback(ctx)

	for _, v := range migrationHistory {
		if v.Version > ver {
			log.Printf("about to run migration %d: %q", v.Version, v.Summary)
			e := UpdateMigrationStatus(ctx, tx, v.Version, v.Summary, false)
			if e != nil {
				return fmt.Errorf("failed to add migration log: %s", e)
			}
			if e = v.Up(ctx, tx); e != nil {
				return fmt.Errorf("failed to run upgrade migration %d: %s", v.Version, e)
			}

			e = UpdateMigrationStatus(ctx, tx, v.Version, v.Summary, true)
			if e != nil {
				return fmt.Errorf("failed to clear flag in migration log: %s", e)
			}
		}
	}
	e = tx.Commit(ctx)
	if e != nil {
		return fmt.Errorf("failed to commit transaction: %s", e)
	}
	return nil
}

// Downgrade calls the downgrade func on the "current" version and update the log
// by deleting the last entry.
func Downgrade(ctx context.Context, pgSrv *db.PGServer, ver int, migrationHistory []*migrations.OpenlyMigration) error {
	tx, e := pgSrv.DBPool.Begin(ctx)
	if e != nil {
		return fmt.Errorf("failed to begin transaction: %s", e)
	}
	defer tx.Rollback(ctx)

	for _, v := range migrationHistory {
		if v.Version == ver {
			log.Printf("about to undo migration %d: %q", v.Version, v.Summary)
			e = UpdateMigrationStatusDown(ctx, tx, v.Version, v.Summary, false)

			if e != nil {
				return fmt.Errorf("failed to add migration log: %s", e)
			}
			if e = v.Down(ctx, tx); e != nil {
				return fmt.Errorf("failed to run upgrade migration %d: %s", v.Version, e)
			}
			e = UpdateMigrationStatusDown(ctx, tx, v.Version, v.Summary, true)
			if e != nil {
				return fmt.Errorf("failed to clrat dirty flag in migration log: %s", e)
			}
			break
		}
	}

	e = tx.Commit(ctx)
	if e != nil {
		return fmt.Errorf("failed to commit transaction: %s", e)
	}
	return nil
}

// ErrDirty is the error we return when there is a migration that didn't finish
var ErrDirty = errors.New("We have a dirty migration, you need to manually fix the error on the database")

// CreatePGSession gives you a db handler to postgres.
func CreatePGSession(pgURI string) (*db.PGServer, context.CancelFunc, error) {

	ctx, cnl := context.WithTimeout(context.Background(), 10*time.Second)

	config, e := pgxpool.ParseConfig(pgURI)
	if e != nil {
		return nil, cnl, fmt.Errorf("failed to parse config: %s", e)
	}
	// TODO: enable this.
	//config.ConnConfig.Logger = &logger.PGLogger{}
	//config.ConnConfig.LogLevel = pgx.LogLevelError

	// I normally add to a project the concept of runmode, local == Dev, then staging and prod, and code does different things under each setup.
	//if mode.CurrRunMode != mode.Dev {
	//	config.ConnConfig.TLSConfig = &tls.Config{}
	//}

	dbpool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		cnl()
		return nil, nil, fmt.Errorf("unable to connect to database: %v", err)

	}

	// check if the PostgreSQL server is alive

	return &db.PGServer{DBPool: dbpool}, cnl, nil
}
