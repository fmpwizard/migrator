package db

import (
	"github.com/jackc/pgx/v4/pgxpool"
)

// PGServer holds the current session to use across all services.
type PGServer struct {
	DBPool *pgxpool.Pool
}
