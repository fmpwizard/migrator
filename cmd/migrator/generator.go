package main

import (
	"fmt"
	"os"
)

var template = `
package migrations

import (
	"context"

	"github.com/jackc/pgx/v4"
)

// Migration%[1]s is the variable we add to the migrationHistory slice of migrations to run
var Migration%[1]s = OpenlyMigration{
	Version: %[2]d,
	Summary: "Add dummy column",
	Up:      up%[2]d,
	Down:    down%[2]d,
}

// up%[2]d implements the upgrade func for the Migration interface
func up%[2]d(ctx context.Context, tx pgx.Tx) error {
	return nil
}

// down%[2]d implements the downgrade func for the Migration interface
func down%[2]d(ctx context.Context, tx pgx.Tx) error {
	return nil
}

`

func createTemplate(ver int) (string, error) {
	// ver is the current latest migration version in the db, so create a +1 version
	ver++
	paddedName := fmt.Sprintf("%06d", ver)
	n := fmt.Sprintf("cmd/migrator/migrations/%06d.go", ver)
	f, e := os.Create(n)
	if e != nil {
		return "", fmt.Errorf("failed to create file %s: %s", n, e)
	}
	_, e = f.Write([]byte(fmt.Sprintf(template, paddedName, ver)))
	if e != nil {
		return "", fmt.Errorf("failed to write content to %s: %s", n, e)
	}
	e = f.Close()
	if e != nil {
		return "", fmt.Errorf("failed to close new migration file %s: %e", n, e)
	}

	return n, nil
}
