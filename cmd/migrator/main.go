package main

import (
	"context"
	"errors"
	"flag"
	"log"
	"os"
	"time"

	"github.com/jackc/pgx/v4"
	"gitlab.com/fmpwizard/migrator/cmd/migrator/migrations"
	"gitlab.com/fmpwizard/migrator/pkg"
)

func main() {
	up := flag.Bool("up", false, "run migration going \"up\" to the latest available migration. This is default false.")
	down := flag.Bool("down", false, "downgrade to the previous migration. Default is false.")
	new := flag.Bool("new", false, "create a new migration file based on a template.")

	// 2 seconds isplenty for this demo app to run migrations.
	ctx, cnl := context.WithTimeout(context.Background(), 2*time.Second)
	defer func() {
		cnl()
	}()

	flag.Parse()

	if *up && *down {
		log.Fatalln("You cannot pass both -up and -down flags, pick one.")
	}

	if !*up && !*down && !*new {
		log.Fatalln("Pass at least -up or -down to migrate your database")
	}

	pgURI, ok := os.LookupEnv("DATABASE_URL")
	if !ok {
		log.Fatalln("the env variable DATABASE_URL is required.")
	}
	pgSrv, pgCancel, err := pkg.CreatePGSession(pgURI)
	if err != nil {
		log.Fatalf("error connecting to PostgreSQL: %s", err)
	}

	defer func() {
		pgCancel()
		pgSrv.DBPool.Close()
	}()

	ver, e := pkg.GetLatestCompletedMigration(ctx, pgSrv)
	if e != nil && e != pgx.ErrNoRows {
		if errors.Is(e, pkg.ErrDirty) {
			log.Fatalln("we have a dirty migration:", e)
		}
		log.Fatalln("failed to get latest migration version: ", e)
	}
	log.Printf("schema is at version: %d ", ver)

	if *new {
		name, e := createTemplate(ver)
		if e != nil {
			log.Fatalf("failed to create new migration: %s", e)
			return
		}
		log.Printf("created new migration at: %s", name)
		return
	}

	if *up {
		e = pkg.RunMigration(ctx, pgSrv, ver, MigrationHistory)
		if e != nil {
			log.Fatalln("failed to run upgrade migrations: ", e)
		}
	}

	if *down {
		e = pkg.Downgrade(ctx, pgSrv, ver, MigrationHistory)
		if e != nil {
			log.Fatalln("failed to run downgrade migrations: ", e)
		}
	}

	log.Println("finished migration")
}

// MigrationHistory is a sorted list of migration we have run.
// Add new migrations to the end of the list
var MigrationHistory = []*migrations.OpenlyMigration{
	&migrations.Migration000001,
	&migrations.Migration000002,
}
