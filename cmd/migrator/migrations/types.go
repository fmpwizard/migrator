package migrations

import (
	"context"

	"github.com/jackc/pgx/v4"
)

// OpenlyMigration is the migration we'll run
type OpenlyMigration struct {
	// Version is this migration's version
	Version int
	// Summary is a short description of this migration
	Summary string
	// Up is the upgrade func
	Up func(context.Context, pgx.Tx) error
	// Down is the downgrade func
	Down func(context.Context, pgx.Tx) error
}
