package migrations

import (
	"context"

	"github.com/jackc/pgx/v4"
)

// Migration000002 is the variable we add to the migrationHistory slice of migrations to run
var Migration000002 = OpenlyMigration{
	Version: 2,
	Summary: "Add dummy column",
	Up:      up2,
	Down:    down2,
}

// up2 implements the upgrade func for the Migration interface
func up2(ctx context.Context, tx pgx.Tx) error {
	sql :=
		`
		ALTER TABLE diego_testing
		ADD COLUMN is_dirty BOOLEAN NOT NULL;
		`
	_, e := tx.Exec(ctx, sql)
	return e
}

// down2 implements the downgrade func for the Migration interface
func down2(ctx context.Context, tx pgx.Tx) error {
	sql :=
		`
	ALTER TABLE diego_testing
	DROP COLUMN is_dirty;
	`
	_, e := tx.Exec(ctx, sql)
	return e
}
