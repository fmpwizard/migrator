package migrations

import (
	"context"

	"github.com/jackc/pgx/v4"
)

// Migration000001 is the variable we add to the migrationHistory slice of migrations to run
var Migration000001 = OpenlyMigration{
	Version: 1,
	Summary: "create dummy table",
	Up:      up1,
	Down:    down1,
}

// up1 implements the upgrade func for the Migration interface
func up1(ctx context.Context, tx pgx.Tx) error {
	sql :=
		`
		CREATE TABLE diego_testing(
		kind varchar not null,
		amount_cents int not null
		);
		`
	_, e := tx.Exec(ctx, sql)
	return e
}

// down1 implements the downgrade func for the Migration interface
func down1(ctx context.Context, tx pgx.Tx) error {
	sql := `DROP TABLE "diego_testing"`
	_, e := tx.Exec(ctx, sql)
	return e

}
